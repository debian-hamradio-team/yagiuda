#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include "yagi.h"

int main(int argc, char **argv)
{
   double r=0.0008, x, real, length, lambda=300/8.05;
   for(length=0.45*lambda; length <=0.52*lambda; length+=0.001*lambda) 
   {
      self(r, length, lambda, &real, &x);
      printf("Z = %f  %f jX \n", length/lambda,real,x);
   }
   printf("\n");
   exit(0);
}
