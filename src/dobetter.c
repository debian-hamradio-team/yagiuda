#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <errno.h>
#include "yagi.h"

/* This routine prints the data to disk and to stdout, since the new result
is better than all previous */

void do_since_better(int i, char *output_filename, char *update_filename, struct FCOMPLEX input_impedance, struct performance_data n,struct flags flag,char * notes,double frequency,double min_frequency,double max_frequency,double  step_frequency,int elements, int driven,int parasitic,double angular_step,double **driven_data,double **parasitic_data,double scale_factor,double new_perf)
{
			static int run_first_time=TRUE;

			FILE *fp_out, *update_fp;
			int print_fitnessQ;
			n.r=input_impedance.r; n.x=input_impedance.i;
			if(flag.Wflg || flag.gflg)
				print_fitnessQ=TRUE;
			else
				print_fitnessQ=FALSE;
			print_relavent_performance_data(stdout,"",i,flag,n,new_perf,TRUE,print_fitnessQ); 
			update_fp=fopen(update_filename,"a");
			if(run_first_time==TRUE && flag.wflg)
				fprintf(update_fp, "Optimised for wide-band use\n");
			if(run_first_time==TRUE && flag.gflg)
				fprintf(update_fp, "Optimised With the genetic algoritm\n");
			run_first_time=FALSE;

			print_relavent_performance_data(update_fp,"",i,flag,n,new_perf,TRUE,print_fitnessQ); 
			fclose(update_fp);
			/* write our best design to date to disk */
			
			fp_out=fopen(output_filename,"wt");
			write_input_data_to_disk(fp_out, notes, frequency/1e6,                          min_frequency/1e6,max_frequency/1e6, step_frequency/1e6, elements,              driven, parasitic, angular_step,driven_data, parasitic_data,                    scale_factor); 
			fclose(fp_out);

#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in dobetter.c\n", errno);
		exit(1);
	}
#endif
}
