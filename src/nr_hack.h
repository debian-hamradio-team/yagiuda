/* Here are the prototypes of the routines that are in the book 'Numerical
recipes in C' by Press et al, 1992. All references to 'float' in that book 
have been changed to 'double' here. Hence the definitions are not the same.
ie a routine:
				 void cisi(float x, float *ci, float *si);

has been changed to:

             void cisi(double x, double *ci, double *si);

Hence all the C routines used here *must* be converted to double. Please *dont*
use the numerical recipes header file 'nr.h', use my 'nr_hacked.h' */


void lubksb(double **a, int n, int *indx, double b[]); 
void ludcmp(double **a, int n, int *indx, double *d); 
void cisi(double x, double *ci, double *si); 

/* END OF NUMERICAL RECIPES ROUTINES */
