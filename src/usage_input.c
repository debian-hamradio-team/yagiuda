#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include "yagi.h"

void usage_input(char *exefile)
{

	fprintf(stderr,"\nYagi-Uda antenna analysis programs, version %.2f\n", version());
	fprintf(stderr,"Written by David Kirkby BSc MSc G8WRB (email:david.kirkby@onetel.net)\n");
	fprintf(stderr, "\nUSAGE: %s  [-h] \n\n", exefile);

	fprintf(stderr, "Where the only option is:\n");
	fprintf(stderr, "   -h     Print this help screen\n\n\n");

	fprintf(stderr,"'input' is an interactive program, that asks the user for formation  about  \n");
	fprintf(stderr,"a  Yagi  design,  such as position of elements, spacing between elements,\n");
	fprintf(stderr,"frequency span of interest and a  filename to  save the data to. It then \n");
	fprintf(stderr,"writes this information into an ASCII file, which can be read by 'yagi' or \n");
	fprintf(stderr,"'optimise'. The file is  ASCII  and  it's format  is  easily  understood\n");
	fprintf(stderr,"by inspection (there are comments printed in it), so it can if necessary\n");
	fprintf(stderr,"be edited manually. After running 'input' and specifying a filename \n");
	fprintf(stderr,"(for example 144e10) you should then type 'yagi 144e10' then 'output 144e10'\n");
	fprintf(stderr,"and then and optionally 'optimise 144e10'.\n");
}

