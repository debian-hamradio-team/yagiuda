#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include "yagi.h"

extern int errno;

void write_header_to_disk(FILE *ofp, int elements, int driven,
int parasitic, double min_frequency, double max_frequency, double frequency,
double step_frequency, double angular_step)
{
	fwrite((char *) &elements, sizeof(elements), 1, ofp);
	fwrite((char *) &driven, sizeof(driven), 1, ofp);
	fwrite((char *) &parasitic, sizeof(parasitic), 1, ofp);
	fwrite((char *)&min_frequency, sizeof(min_frequency), 1, ofp);
	fwrite((char *)&max_frequency,sizeof(max_frequency), 1, ofp);
	fwrite((char *)&frequency, sizeof(frequency), 1, ofp);
	fwrite((char *)&step_frequency, sizeof(step_frequency), 1, ofp);
	fwrite((char *)&angular_step, sizeof(angular_step), 1, ofp);
	fseek(ofp, HEADER_SIZE,SEEK_SET);

#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in header.c\n", errno);
		exit(1);
	}
#endif
}
