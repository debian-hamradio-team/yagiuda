#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "yagi.h"

extern int errno;

int get_number_of_elements(char *input_filename, int *driven , int *parasitic)
{
	FILE *ifp;
	char *null, *line;
	int num_elements;

	null=string(0L,12L);
	line=string(0L,81L);
	ifp=fopen(input_filename, "rt");
	*driven=-1;
	*parasitic=-1;
	if(ifp == NULL)
	{
		fprintf(stderr,"Sorry, cant find file:  %s\n", input_filename);
		exit(2);
	}
	/* Read line by line, looking for data on number of elements */
	while(!feof(ifp))
	{
		fgets(line, 80, ifp);
		if(strncmp(line,"ELEMENTS",8) == 0)  
		{
			sscanf(line,"%s %d\n", null, &num_elements);						
		}
		if(strncmp(line,"DRIVEN",6) == 0)  
		{
			sscanf(line,"%s %d\n", null, driven);						
		}
		if(strncmp(line,"PARASITIC",8) == 0)  
		{
			sscanf(line,"%s %d\n", null, parasitic);						
		}
	}
#ifdef DEBUG
	errno=0;
#endif
	fclose(ifp);
	if(num_elements == -1)
		fprintf(stderr,"Error in data file %s: ELEMENTS undefined\n",input_filename); 
	if(*driven == -1)
		fprintf(stderr,"Error in data file %s : DRIVEN undefined\n", input_filename); 
	if(*parasitic == -1)
		fprintf(stderr,"Error in data file %s: PARASITIC undefined\n", input_filename); 
	if(num_elements != *driven + *parasitic)
	{
		fprintf(stderr,"Check ELEMENTS, DRIVEN & PARASITIC in data file %s\n", input_filename);
		fprintf(stderr,"ELEMENTS = %d DRIVEN = %d PARASITIC = %d\n", num_elements,      *driven, *parasitic);
		exit(3);
	}
	free_string(line,0L,81L);
	free_string(null,0L,12L);

#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in mum_elem.c\n", errno);
		exit(1);
	}
#endif
	return(num_elements);
}

