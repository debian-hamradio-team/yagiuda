#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <errno.h>
#include "yagi.h"

extern int errno;

void copy_matrix(int length, int width, double **to, double **from) 
{
	int i,j;
	for(i=1;i<=length;++i)
	{
		for(j=1;j<=width;++j)
		{
			to[i][j]=from[i][j];
		}
	}

#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in copy_matrix() of copym.c\n", errno);
		exit(1);
	}
#endif
}

/* The following functions copies data from a matrix z containing real and
imaqginary data to a much larger real matrix A, so that simultaneouis
equations can be solved by the method in 'Numerical Recipes in C' 

		put the data in the form:
	
		|Zr -Zi|   |Ir|   = |Vr|   which we will call A.x=b
		|Zi  Zr|   |Ii|     |Vi| 
		
		so the Z data goes now in a 2Nx2N matrix. This is a bit wasteful of space
		and time, but it will do here. */

void copy_complex_data_to_real_matrix(int elements, double **z, double **A)
{
		int i, j;
		for(i=1;i<=elements;++i)
		{
			for(j=1;j<=elements;++j)
			{
				A[i][j]=z[i][2*j-1];   /* real data, top left corner of Z */
				A[i+elements][j+elements]=z[i][2*j-1]; /* Bot right, real data */
				A[i][j+elements]=-z[i][2*j]; /* imaginary, top right */
				A[i+elements][j]=z[i][2*j]; /* bottom left, imaginary */
			}
		}
#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in copy_complex_data_to_real_m() of copym.c\n", errno);
		exit(1);
	}
#endif
}
