#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#ifndef i386
 
#endif
#include <math.h>
#include <errno.h>
#include "yagi.h"
extern int errno;

void self_impedance(int i, double frequency, int driven, int parasitic, double **data, double **impedance)
{
	int j, row, column_real, column_imag, max_data, ii;
	double lambda,  x , length,  diameter;
	double radius, real;

	ii=i;
	/* Put the complex value of impedance for each element into
	the array. This is a complex, as the array sizes for
	driven and parasitic elements are different */

	/* first compute the wavelength */
	lambda=3e8/frequency;

	row=i; 				   /* write data to row, column_real_or_imag */
	column_real=2*i-1;
	column_imag=2*i;
	if(i <= driven)
	{
		max_data=MAX_DRIVEN;
	}
	else if (i > driven)    			 /* parasitic element */
	{
		max_data=MAX_PARASITIC;
		i-=driven;							/* read from i-driven */
	}
	diameter=data[i][DIAMETER];
	length=data[i][LENGTH];
	j=2*ii-1;
	/* Use method in 'Antenna Theory' Baluns ' */
	radius=diameter/2.0;   /* radius */
	self(radius,length,lambda,&real,&x);
	impedance[row][column_imag]= x; /* write imaginary data to array */
	impedance[ii][j]= real;  
}
