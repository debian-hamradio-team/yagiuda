#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
/* file ck_flags.c */
#include <errno.h>
#include <stdio.h>
#include "yagi.h"

/* This function checks that all the options are sensible */

extern int errno;

void check_flags(struct flags flag, int argc, int optind, char *exefilename)
{
	if(flag.errflg==1)
		usage_optimise(exefilename);		
	if(flag.hflg==1) /* help flag */
		usage_optimise(exefilename);		
	if(flag.dflg==1)
		show_all_optimise_parameters(exefilename,flag);

	if((flag.gflg==1) && (flag.Wflg==1))
	{
		error_message("\nThe genetic algorithm invoked with the '-g' option must, due to the very nature of genetic algorithms, use a weighted combination of parameters. You need not (nor are you allowed too) specify the '-W' and '-g' options together.\n");
		exit(1);
	}
	if(flag.gflg && flag.rflg)
	{
		error_message("\nYou can not specify the genetic algorithm flag '-g' and also specify the resistance flag '-r' at the same time.\n"); 
		exit(1);
	}
	if(flag.gflg && flag.sflg)
	{
		error_message("\nYou can not specify the genetic algorithm flag '-g' and also specify the swr flag '-s' at the same time. To alter the genetic algorithms behaviour with reguard to swr, change the swr weight with the '-S' flag.\n"); 
		exit(1);
	}
	if(flag.gflg && flag.lflg)
	{
		error_message("\nYou can not specify the genetic algorithm flag '-g' and also specify the flag '-l' at the same time.\n"); 
		exit(1);
	}
	if( (flag.tflg && !flag.Tflg) || (flag.Tflg && !flag.tflg))
	{
		error_message("\nThe options '-t' and '-T' must both be used together. They can not be used with any other options\n");
		exit(1);
	}
	if( flag.tflg &&( flag.bflg || flag.cflg || flag.fflg ||
		 flag.gflg || flag.hflg || flag.oflg || flag.pflg || flag.lflg ||
		 flag.rflg || flag.sflg || flag.xflg || flag.Fflg ||
		 flag.Gflg || flag.Rflg || flag.Cflg || flag.Sflg || flag.Wflg   
		 ) )
	{
		error_message("The flags '-t' and '-T' can not be used with one of the other options specified. The only option allowed with them is '-Z' to fix the charaterisitc impedance, '-d'  or '-v'\n");
		exit(1);
	}
	if(flag.cflg && !(flag.gflg || flag.Wflg) )
	{
		error_message("The flag '-c' can only be used with the '-W' or '-g' flags\n");
		exit(1);
	}
	if((flag.Fflg || flag.Gflg || flag.Rflg || flag.Pflg || flag.Sflg || flag.Xflg) && !(flag.gflg || flag.Wflg))
	{
		error_message("The weight flags (F, G, R, P, S and X) must be used either with the genetic algorithm flag (-g) or the the weithted performance flag (-W)\n");
		exit(1);
	}
	if(flag.gflg && flag.wflg)
	{
		error_message("Sorry, the genetic algorithm can't optimise over a wide band yet, so you cant use the -w option\n");
		exit(1);
	}
	if(flag.pflg && ! flag.gflg)
	{
		error_message("The '-p' population size option is only used with the genetic algorithm '-g' option\n");
		exit(1);
	}
if( (argc - optind) != 3 &&  (argc-optind) !=2)
		usage_optimise(exefilename);

#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in ck_flags.c\n", errno);
		exit(1);
	}
#endif
}
