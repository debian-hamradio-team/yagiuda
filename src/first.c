#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "yagi.h"

/* This program 'first' has a go at a first attempt of a yagi. It uses
as a basis, the DL6WU Yagis, which are strictly only  okay for 10 or more
elements, but this program takes liberties and calculates for any size of
dipole. 

The program 'first' disscussed in an article sent to RadCom, used a much
worst method, by trying to extend a 33 ele beam form the ARRL handbook in
both directions. I've abandoned this algorithm (if you could call it an   
algorithm) in faviour of the DL6WU designs. 

*/

extern int optind, opterr;
extern char *optarg;

/* space between the eleemnts */
double space[]={0.000,0.200,0.075,0.180,0.215,0.250,0.280,0.300,0.315,0.330,0.345,0.360,0.375,0.390,0.400,0.400,0.400,0.4,0.4,0.4,0.4,0.4,0.4,0.4,0.4,0.4,0.4,0.4,0.4,0.4,0.4,0.4,0.4,0.4,0.4};
/* elements length. These values are for a diamter of 0.01 lambda */
double length[]={0.480,0.456,0.425,0.42,0.414,0.408,0.404,0.399,0.396,0.392,0.390,0.388,0.385,0.383,0.381,0.379,0.377,0.376,0.375,0.374,0.372,0.371,0.370,0.369,0.368,0.367,0.366,0.365,0.364,0.363};

/* We now compute the element lengths. The program computes the self reactance 
of eleemnts assuming a diamter of 0.01 lambda, then adjest the length for the 
array 'length' so they have the same reactance. The resistance will change
somewhat, so you cant expect the performance  to be identical with different
element lengths */

int main(int argc, char **argv)
{
	double min_f, f, max_f, step_f, angular_step=180, diameter, lambda;
	double **d, **p,scale_factor=1.0;
	int elements, driven, parasitic, i,c; 
	FILE *fp;
	char *output_filename, *notes="Automatically produced by first";


   while ((c =  getoptions(argc,argv,"m")) != -1)
   switch       (c) 
   {
	}
	if((argc-optind!=7))
	{
		usage_first(argv[0]);
		exit(1);
	}
	elements=atoi(argv[optind+1]);
	if(elements <10)
	{
		printf("DL6WU antennas are only valid for 10 or more elements, but I'll do it!\n");
	}
	min_f=atof(argv[optind+2]);
	f=atof(argv[optind+3]);
	max_f=atof(argv[optind+4]);
	step_f=atof(argv[optind+5]);
	diameter=1e-3*atof(argv[optind+6]);
	driven=1;
	parasitic=elements-1;
	output_filename=string(0L,100L);
	d=dmatrix(1L,(long) driven,1L, 6L);
	p=dmatrix(1L,(long) parasitic,1L, 4L);
	lambda=300/f;
	fp=fopen(*(argv+1),"wt");
	d[1][X]=space[1]*lambda;
	d[1][DIAMETER]=diameter;
	d[1][LENGTH]=new_length(lambda*length[1],0.01*lambda,lambda,diameter);
	d[1][VOLTAGE_R]=1.0;
	d[1][VOLTAGE_I]=0.0;
	/* now all the directors */
	for(i=1;i<=parasitic;++i)
	{
		if(i==1)
		{
			p[1][X]=space[0]; /* position of reflector */
			p[1][DIAMETER]=diameter; /* length of reflector */
			p[1][LENGTH]=new_length(lambda*length[0],0.01*lambda,lambda,diameter);
		}
		else if (i==2)
		{
			p[i][X]=space[i]*lambda+d[1][X];
			p[i][DIAMETER]=diameter; /* length of first director */
			p[i][LENGTH]=new_length(lambda*length[2],0.01*lambda,lambda,diameter);
		}
		else if ((i>2) && (i < 26))
		{
			p[i][X]=space[i]*lambda+p[i-1][X];
			p[i][DIAMETER]=diameter; /* length of subsequent directors */
			p[i][LENGTH]=new_length(lambda*length[i],0.01*lambda,lambda,diameter);
		}
		else if(i>=26)
		{ 
			p[i][X]=0.400*lambda+p[i-1][X];
			p[i][LENGTH]=0.997*p[i-1][LENGTH]; /* Log taper, eancjh element constant fraction of the length od the last one */
			p[i][DIAMETER]=diameter; /* length of these, assume constant frac of previous */
		}
	}
	write_input_data_to_disk(fp, notes, f, min_f, max_f, step_f, elements   , driven, parasitic, angular_step, d, p, scale_factor); 
	fclose(fp);
	free_string(output_filename,0L,100L); 
	exit(0);
}
