#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include "yagi.h"
/* This function set the structures to have sensible values. They
often get altered, by command line options */
void set_performance_structures(struct performance_data *weight, struct \
performance_data *max, struct performance_data *best, struct performance_data *worst)
{
	weight->swr=1.0; /* initial weights. Use can change */
	weight->fb=1.0;
	weight->sidelobe=1.0;
	weight->gain=1.0;

	max->fb=REASONABLE_FB;   /* Initial max values worth getting.  */
	max->swr=REASONABLE_SWR; /* ie reasonable values, set in yagi.h */
	max->r=REASONABLE_R;    /* user can change these with options */
	max->x=REASONABLE_X;    /* such as -fy, -sy, -ry, -xy, -py */
	max->sidelobe=REASONABLE_SIDELOBE; /* where 'y' is a double */

	best->swr=99.0; /* Assumed values of the staring antenna ie. poor antenna */
	best->gain=-1000.0;
	best->fb=-1000.0;
	best->r=1000.0;
	best->x=1e19;
	best->sidelobe=-1000.0;

	worst->gain=1000.0;
	worst->sidelobe=1000.0;
	worst->fb=1000.0;
	worst->swr=1.00;	

}
