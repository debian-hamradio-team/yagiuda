#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include "yagi.h"

extern int errno;

void end_if_stop_exists(int *i, int iterations,int divisor)
{
	if(*i%divisor==0)
	{
		if(fopen("stop","rt") != NULL)
			*i=iterations;  /* force it to exit very soon */
		errno=0; /* Will be set to non zero if file dont exist */
		printf("%d\r", *i);
	} 
}

