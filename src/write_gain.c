#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include "yagi.h"
extern int errno;


void write_gain_at_various_angles(FILE *gain_fp, double angular_step, double pin, double normalised_f, double f, struct element_data *coordinates, struct FCOMPLEX *current, int elements, double design_f)
{
	double theta, phi, gain_E_plane, gain_H_plane, E_plane_gain, H_plane_gain;
	double peak_gain;
	static int run_first_time=0;
		fprintf(gain_fp,"#  f(MHz)     theta   gain-E(dBi)   G(E)-peak   phi    gain-H(dBi)  G(H)-peak\n");
		run_first_time=1;
	gain(90.0,0,pin,normalised_f,coordinates, current, elements, &gain_E_plane, &peak_gain, f, design_f);
	for(phi=-180;phi <=180.0;phi+=angular_step)
	{
			theta=90+phi; /* 	Just helps program a bit. I'm not saying it is!! */
			/* compute gain(phi, 90) */

			gain(90.0,phi+360,pin,normalised_f,coordinates, current, elements, &gain_E_plane, &gain_H_plane, f, design_f);

			H_plane_gain=gain_H_plane;

			theta=90+phi; /* 	Just helps program a bit. I'm not saying it is!! */
			/* compute gain(theta,0) where thete =90-270 degrees */
			gain(theta+360,0,pin,normalised_f,coordinates, current, elements, &gain_E_plane, &gain_H_plane, f, design_f);
			E_plane_gain=gain_E_plane;
			fprintf(gain_fp,"%10.4f %10.4f %10.4f %10.4f %10.4f %10.4f %10.4f\n",f/1e6, theta, E_plane_gain, E_plane_gain-peak_gain, phi, H_plane_gain, H_plane_gain-peak_gain);
	}

#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in write_ga.c\n", errno);
		exit(1);
	}
#endif
}

