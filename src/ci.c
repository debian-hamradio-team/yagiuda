#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <math.h>
#include "yagi.h"

double ci(double x)			/* cosine integral */
{
	double null, result;

	cisi(x, &result, &null);
	return(result);
}
