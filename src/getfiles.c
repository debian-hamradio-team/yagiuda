#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "yagi.h"

extern int errno;

/* This routine checks the number of arguments. If thats OK, the 
only arguemnt is the input data file. The output data file name is
got by adding '.out' to the input data file name. */
char *get_data_filenames(int optind, char **argv, char *input)
{
	char *output;
	output=string(0L,100L);
	strcpy(input, argv[optind]);
	strcpy(output, argv[optind]);
	strcat(output,".out");

#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in get_data_filenames() of getfiles.c\n", errno);
		exit(1);
	}
#endif
	return(output);
}
