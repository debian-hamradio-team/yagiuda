#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include "yagi.h"

void set_mean_structure(struct FCOMPLEX input_impedance, \
double E_fwd, double E_back,struct flags flag, double pin,struct element_data *coordinates, struct FCOMPLEX *current, int elements, double frequency, double design_frequency, struct performance_data *mean_performance)
{
	
	double magnitude,phase,vswr;
	int choice;
	choice=flag.oflg;
	reflection_coefficient(input_impedance,&magnitude,&phase);
	vswr=calculate_vswr(magnitude);
	mean_performance->gain=+E_fwd;
	mean_performance->fb=+(E_fwd-E_back);
	mean_performance->swr=+vswr;
	mean_performance->r=input_impedance.r;   /* in Ohms */
	mean_performance->x=input_impedance.i;   /* in Ohms */
	if((choice&SIDE_LOBE_LEVEL)==SIDE_LOBE_LEVEL  || flag.Tflg || ((flag.Wflg&SIDE_LOBE_LEVEL)==SIDE_LOBE_LEVEL) )
		mean_performance->sidelobe+=find_max_sidelobe_fast(E_fwd, pin,coordinates,current,elements,frequency,design_frequency);
	else
		mean_performance->sidelobe=0.0;
}

