#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include "yagi.h"
extern int errno;

int read_header(FILE *ifp, FILE *ofp, double *min_f, double *max_f,
	 double *step_f, double *f, double *angular_step)
{
	int elements, driven, parasitic;

	fread((char *) &elements, sizeof(elements), 1, ifp);
	fread((char *) &driven, sizeof(driven), 1, ifp);
	fread((char *) &parasitic, sizeof(parasitic), 1, ifp);
	fread((char *) min_f, sizeof(*min_f), 1, ifp);
	fread((char *) max_f,sizeof(*max_f), 1, ifp);
	fread((char *) f, sizeof(*f), 1, ifp);
	fread((char *) step_f, sizeof(*step_f), 1, ifp);
	fread((char *) angular_step, sizeof(*angular_step), 1, ifp);
	fseek(ifp, HEADER_SIZE, SEEK_SET);             /* skip rest of header */
	/* now we make some basic checks on the header, to see nothing is too
	far wrong. */
	if( (*max_f < *min_f) || *angular_step==0 || driven+parasitic != elements || (*step_f >  *max_f))
	{
		fprintf(stderr,"Error in input file\n");
		fprintf(stderr,"driven=%d parasitic=%d elements=%d\n",driven,parasitic,elements);
		fprintf(stderr,"min_f=%f max_f=%f step_f=%f\n",*min_f, *max_f, *step_f);
		fprintf(stderr,"angular_step=%f \n", *angular_step);
		exit(19);
	}
	fprintf(ofp,"# Driven=%d parasitic=%d total-elements=%d design=%.3fMHz\n", driven, parasitic, elements,  *f/1e6);
	fprintf(ofp,"# Checked from %.3fMHz to %.3fMHz.\n", *min_f/1e6, *max_f/1e6);

#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in read_hea.c\n", errno);
		exit(1);
	}
#endif
	return(elements);
}
