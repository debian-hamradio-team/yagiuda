#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include "yagi.h"
extern int errno;

/* compute the input impedance (driving impedance) */

void z_input(struct FCOMPLEX v, struct FCOMPLEX i, struct FCOMPLEX *impedance)
{
	*impedance=Cdiv(v,i);
       /* printf("Voltage=%f + i %f\n", v.r, v.i);
	printf("Current = %f + i %f \n", i.r, i.i); 
	printf("Z = V/I = %f + %f \n", impedance->r, impedance->i); */

#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in z_input() in z_input.c\n", errno);
		exit(1);
	}
#endif
}

