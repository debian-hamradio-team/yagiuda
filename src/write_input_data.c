#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include "yagi.h"

void write_input_data_to_disk(FILE *fp, char *notes, double frequency, double min_f,double max_f, double step_f, int elements,int driven,int parasitic, double angular_step, double **d, double **p, double scale_factor) 
{
	int i; 
	fprintf(fp, "NOTES %s\n", notes);
	fprintf(fp, "FREQUENCY %f\n", frequency);
	fprintf(fp, "MIN_FREQUENCY %f\n", min_f);
	fprintf(fp, "MAX_FREQUENCY %f\n", max_f);
	fprintf(fp, "STEP_FREQUENCY %f\n", step_f);
	fprintf(fp, "ELEMENTS %d\n", elements);
	fprintf(fp, "DRIVEN %d\n", driven);
	fprintf(fp, "PARASITIC %d\n", elements-driven);
	fprintf(fp, "ANGULAR_STEP   %f\n", angular_step);
	fprintf(fp, "#DATA_DRIVEN        x         y       length     diameter voltage(r) voltage(i)\n");
	fprintf(fp, "DATA_DRIVEN ");
	for(i=1; i<=driven; ++i)
		fprintf(fp," %10.5f %10.5f %10.5f %10.5f %10.5f %10.5f", d[i][1]/scale_factor, d[i][2]/scale_factor, d[i][3]/scale_factor, d[i][4]/scale_factor, d[i][5], d[i][6]);
	fprintf(fp, "\n#DATA_PARASITIC     x         y       length     diameter\n");
	fprintf(fp, "DATA_PARASITIC\n");
	for(i=1; i<=elements-driven; ++i)
	{
	if(i==1)
		fprintf(fp,"             %10.5f %10.5f %10.5f %10.5f reflector\n", p[i][X]/scale_factor, p[i][Y]/scale_factor, p[i][LENGTH]/scale_factor, p[i][4]/scale_factor);
	else
		fprintf(fp,"             %10.5f %10.5f %10.5f %10.5f D%d\n", p[i][X]/scale_factor, p[i][Y]/scale_factor, p[i][LENGTH]/scale_factor, p[i][4]/scale_factor,i-1);
	}
}
