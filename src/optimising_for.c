#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include "yagi.h"

extern struct performance_data max;

void optimising_for(struct flags flag)
{
	int choice; 
	if(flag.gflg)
	{
		printf("Will optimise using a genetic algorithm\n");
		choice=flag.gflg;
	}
	else if(flag.Wflg)
	{
		printf("Will optimise using a weighted combination of parameters\n");
		choice=flag.Wflg;
	}
	else if(flag.wflg)
	{
		printf("Will optimise over a wide band\n");
		choice=flag.wflg;
	}
	else
	{
		printf("Will optimise insisting all selected parameters improve.\n");
		choice=flag.oflg;
	}
	if((choice&GAIN)==GAIN)
		printf("Optimising for maximum possible gain.\n");
	if((choice&FB)==FB && (choice&REASONABLE)==0)
		printf("Optimising for maximum possible FB ratio.\n");
	if((choice&FB)==FB && (choice&REASONABLE)==REASONABLE)
		printf("Optimising for FB ratio to a maximum of %.3fdB.\n",max.fb);
	if((choice&RESISTANCE)==RESISTANCE && (choice&REASONABLE)==0)
		printf("Optimising for an input resistance as close as possible to Z0.\n");
	if((choice&RESISTANCE)==RESISTANCE && (choice&REASONABLE)==REASONABLE)
		printf("Optimising for an input resistance of Z0 +/- %.3f Ohms\n",max.r);
	if((choice&REACTANCE)==REACTANCE && (choice&REASONABLE)==0)
		printf("Optimising for an input reactance as close as possible to zero.\n");
	if((choice&REACTANCE)==REACTANCE && (choice&REASONABLE)==REASONABLE)
		printf("Optimising for an input reactance of 0 +/- %.3f Ohms\n",max.x);
	if((choice&VSWR)==VSWR && (choice&REASONABLE)==REASONABLE)
		printf("Optimising for an input VSWR of less than %.3f:1\n",max.swr);
	
	if((choice&VSWR)==VSWR && (choice&REASONABLE)==0)
		printf("Optimising for an input VSWR as low as possible\n");
	
	if((choice&SIDE_LOBE_LEVEL)==SIDE_LOBE_LEVEL && (choice&REASONABLE)==0)
		printf("Optimising for the most significant side to be as small as possible.\n");
	if((choice&SIDE_LOBE_LEVEL)==SIDE_LOBE_LEVEL && (choice&REASONABLE)==REASONABLE)
		printf("Optimising for the most significant sidelobe to be at least %.3f dB down\n",max.sidelobe);
}

