#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include "yagi.h"

void usage_yagi(char *exefile)
{
	
		fprintf(stderr,"\nYagi-Uda antenna analysis programs, version %.2f\n", version());
		fprintf(stderr,"Written by Dr. David Kirkby Ph.D. G8WRB (email:david.kirkby@onetel.net)\n");
		fprintf(stderr, "\nUSAGE: %s  [-dhps] filename \n\n", exefile);
		fprintf(stderr, "Where options are:\n");
		fprintf(stderr, "   -d     Display  bar-graphs, proportional to element currents\n");
		fprintf(stderr, "   -h     Print this help screen\n");
		fprintf(stderr, "   -p     Print Z matrix to screen.\n");
		fprintf(stderr, "   -s     Suppress diagnostic output\n");
		fprintf(stderr, "'yagi' computes the currents at the centre of of each element of an antenna \ndescribed in 'filename', where 'filename' was created by 'input' or 'first'.\n");
		fprintf(stderr, "After running 'yagi filename' type 'output filename'\n");
}
