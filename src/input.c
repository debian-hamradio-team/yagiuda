#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

/* This program, called 'input' asks the user for information about the
yagi. This includes the number of elements, design frequency, dimension and
locations of elements.  It also asks for a range of frequencies to plot
the antenna details over (eg 144-146MHz) and an angular step size (eg 5deg)
over which the antenna pattern is plotted in both theta and phi (in this case
0deg, 5deg, 10deg, 15 deg ... 180deg). This program just writes all this
information into a file, which can be read by 'yagi'. The file created by
this program has the extension .out and is a text file, so it can be editied 
manually if required. Be warned though,  'yagi' may not report errors in the
.out file, so extreme care is necessary if the .out file file is edited
manually! 
*/

#include <stdio.h>
#include <malloc.h>
#include <math.h>
#include "nrutil.h"
#include "yagi.h"

extern int optind, opterr;

int main(int argc, char **argv)
{
	double frequency, diameter, **d, **p;
	double min_f, max_f, step_f, angular_step;
	int elements, driven, i, fixed_diameter, boom, parasitic, c, hflg=0, errflg=0;
	double scale_factor;
	char response, *filename, *notes, *type, data_entry;
	FILE *fp;

	filename = string(0L, 1000L);
	notes = string(0L, 1000L);
	type = string(0L, 1000L);

   while ((c =	getoptions(argc,argv,"h")) != -1)
   switch	(c) 
   {
			 case 'h':  /* help */
				hflg=1;
			      	break;
			 case '?':   /*  */
				errflg=1;
				break;
	}
	if(hflg || errflg)
	{
		usage_input(argv[0]);
		exit(0);
	}
	printf("Yagi-Uda antenna analysis programs, version %.2f\n", version());
	printf("Written by Dr. David Kirkby Ph.D. (G8WRB, email:david.kirkby@onetel.net)\n");
	printf("\nThis program asks for length, diameter and position of antenna elements then\n");
	printf("writes them to a file you specify. Data is written in m (metres)\n \n");
	printf("Enter any notes on this design (up to 400 characters): ");
	gets(notes);
	printf("Enter a filename to write data to ");
	gets(filename);
	printf("Enter the centre frequency in MHz ");
	scanf("%lf", &frequency);
	printf("Enter the minimum frequency in MHz ");
	scanf("%lf", &min_f);
	printf("Enter the maximum frequency in MHz ");
	scanf("%lf", &max_f);
	printf("Enter the frequency steps in MHz ");
	scanf("%lf", &step_f);
	if(min_f > frequency)
		nrerror("The minimum frequency has been set higher than the centre frequency");
	if(max_f < frequency)
		nrerror("The maximum frequency has been set lower than the centre frequency");
	printf("\nData can be entered in imperial (inches) metric (mm) or wavelengths (lambda)\n");
	printf("Please enter i (for imperial), m (for metric) or w (for wavelengths). ");
	scanf("%c", &data_entry);
	scanf("%c", &data_entry);
	/* The file produced by this is always written in metres.  */
	if(data_entry=='i' || data_entry=='I')
	{
		scale_factor=39.37; /* inches in 1m */
		type="\"\"";
	}
	else if(data_entry=='m' || data_entry=='m')
	{
		scale_factor=1000.0; /* mm in 1m */
		type="mm";
	}
	else if(data_entry=='w' || data_entry=='w')
	{
		scale_factor=frequency/300; /* wavelegths in 1m */
		type="lambda";
	}
	else
		exit(1);
	printf("Enter the number of elements for the antenna ");
	scanf("%d",&elements);
	/* printf("Enter the number of driven elements ");
	scanf("%d", &driven); */
	driven=1;
	parasitic=elements-driven;
	d=dmatrix(1L, (long) driven, 1L, 6L);
	p=dmatrix(1L, (long) parasitic , 1L, 4L);
	printf("Enter the angular step size in degrees for the gain to be plotted ");
	scanf("%lf",&angular_step);
	/* printf("Are any of the elements tappered? ");
	scanf("%c", &response);
	scanf("%c", &response);
	if(response =='y' || response == 'Y')
	{
		tappered=TRUE;
		printf("\nSorry: this program as yet can't handle tapered elements.\n");
		exit(1);
	}
	*/
	printf("Are all the elements of the same diameter ? ");
	scanf("%c",&response);
	scanf("%c",&response);
	if(response=='Y' || response =='y')
	{
		printf("Enter the diameter of the elements (in %s) ", type);
		scanf("%lf", &diameter);
		fixed_diameter=TRUE;
	}
	else
		fixed_diameter=FALSE;
	boom=TRUE;
	for(i=1;i<=driven;++i) 
	{
		printf("\n\nEnter location of the driven element in %s (wrt. reflector at x=0) ", type);
		scanf("%lf", &d[i][X]);
		d[i][Y]=0;	
		if( boom != TRUE)
		{
			printf("Enter the y position of the driven element (in %s) ", type);
			scanf("%lf", &d[i][Y]);
		}
		else
			d[i][Y]=0.0;
		printf("Enter length of the driven element (in %s) ", type);
		scanf("%lf", &d[i][LENGTH]);
		if(fixed_diameter == TRUE)
			d[i][DIAMETER]=diameter;
		else
		{
			printf("Enter the diameter of the driven element (in %s) ", type);
			scanf("%lf", &d[i][DIAMETER]);
		}
		/*
		printf("Enter the voltage driving this element ");
		scanf("%lf", &d[i][VOLTAGE_R]);
		printf("Enter the phase driving this element ");
		scanf("%lf", &d[i][VOLTAGE_I]);
		*/
		d[i][VOLTAGE_R]=1.0;
		d[i][VOLTAGE_I]=0.0;
	}
	for(i=1;i<=elements-driven;++i) 
	{
	        if(i==1) /* The reflector */
		{
		   printf("\nEnter length of the reflector (in %s) ", type);
		   scanf("%lf", &p[1][LENGTH]);
		   if(boom != TRUE)
		   {
			printf("\nEnter the y position of the reflector (in %s) ", type);
			scanf("%lf", &p[i][Y]);
		   }
		   if(fixed_diameter == TRUE)
			p[i][DIAMETER]=diameter;
		   else
		   {
			printf("Enter the diameter of the reflector (in %s) ", type);
			scanf("%lf", &p[i][DIAMETER]);
		   }
		}
		else /* a director */
		{
		   printf("\n\nEnter location of director %d in %s (wrt. reflector at x=0.0) ", i-1, type);
		   scanf("%lf", &p[i][X]);
		   if( boom != TRUE)
		   {
			printf("Enter the y position of director %d (in %s) ", i-1, type);
			scanf("%lf", &p[i][Y]);
		   }
		   else
			p[i][Y]=0.0;
		   printf("Enter the length of director %d (in %s) ", i-1,type);
		   scanf("%lf", &p[i][LENGTH]);
		   if(fixed_diameter == TRUE)
			p[i][DIAMETER]=diameter;
		   else
		   {
			printf("Enter the diameter of director %d (in %s) ", i-1, type);
			scanf("%lf", &p[i][DIAMETER]);
		   }
		}
	}
	/* Now write data to disk */
	fp=fopen(filename,"wt");
	write_input_data_to_disk(fp, notes, frequency, min_f, max_f, step_f, elements   , driven, parasitic, angular_step, d, p, scale_factor); 
	fclose(fp);
	free_string(filename,0L, 1000L);
	free_string(notes,0L, 1000L);
	free_string(type,0L, 1000L);
	free_dmatrix(d, 1L, (long) driven, 1L, 6L);
	free_dmatrix(p, 1L, (long) parasitic , 1L, 4L);
	exit(0);
}

