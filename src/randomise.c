#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include <errno.h>
#ifdef sun

#endif
#include "yagi.h"

extern double boom_factor;
extern int errno;
#define MIN_RDS   0.05 /* Minimum reflector-driven spacing in lambda */

void randomise(int randomisation_method, double frequency, double max_percent, double **driven_data, double**parasitic_data, int driven, int parasites)
{
	int i,elements;
	static int run_first_time=0;
	double y, old_boom_length, new_boom_length, a, lambda,**z,l,x,taper;
	static double max_boom_length;
	lambda=300/frequency;
	if(run_first_time==0)
	{
		max_boom_length=(1+boom_factor/100)*parasitic_data[parasites][X];
	}
	run_first_time++;
	if ((randomisation_method & DRIVEN_LENGTH) == DRIVEN_LENGTH) /* 1 */
	{
		for(i=1;i<=driven; ++i) /* randomise the driven */
		{
			y=1+0.02*max_percent*(randreal()-0.5);
			driven_data[i][LENGTH]*=y;
		}
	}
	if ((randomisation_method & DRIVEN_POSITION) == DRIVEN_POSITION) /* 2 */
	{
		for(i=1;i<=driven; ++i) /* randomise the driven position */
		{
			y=1+0.02*max_percent*(randreal()-0.5);
			driven_data[i][X]*=y;
		}
	}
	if ((randomisation_method & REFLECTOR_LENGTH) == REFLECTOR_LENGTH) /* 4 */
	{

	   /* reflector position stays at x=0 */
		y=1+0.02*max_percent*(randreal()-0.5);
		parasitic_data[1][LENGTH]*=y;
	}

	if ((randomisation_method & DIRECTOR_LENGTH) == DIRECTOR_LENGTH) /* 8 */
	{
		for(i=2; i<=parasites; ++i)
		{
			y=1+0.02*max_percent*(randreal()-0.5);
			parasitic_data[i][LENGTH]*=y;
			/* make sure directors dont get longer down the boom */
			if((parasitic_data[i][LENGTH]>parasitic_data[i-1][LENGTH]) && (i>2))
				parasitic_data[i][LENGTH]=parasitic_data[i-1][LENGTH]; 
		}
	}

	if ((randomisation_method & DIRECTOR_POSITION) == DIRECTOR_POSITION) /* 16 */
	{
		old_boom_length=parasitic_data[parasites][X];    
		y=1+0.02*max_percent*(randreal()-0.5);
		new_boom_length=old_boom_length*y;
		if(new_boom_length > max_boom_length)
			new_boom_length=max_boom_length;
		/* rearrange directors */
		for(i=2; i<=parasites; ++i)
		{
			y=1+0.02*max_percent*(randreal()-0.5);
			parasitic_data[i][X]*=y*(new_boom_length/old_boom_length);
		}
	}
	if ((randomisation_method & ALL_ELEMENT_LENGTHS_IDENTICAL)==ALL_ELEMENT_LENGTHS_IDENTICAL) /* 32 */
	{
			y=1+0.02*max_percent*(randreal()-0.5);
			for(i=1; i<=driven; ++i)
				driven_data[i][LENGTH]*=y;
			for(i=1; i<=parasites; ++i)
				parasitic_data[i][LENGTH]=driven_data[1][LENGTH];
	}
	if ((randomisation_method & LINEAR_TAPER)==LINEAR_TAPER) /* 64 */
	{
		a=1+(0.1*randreal()); /* 1.00 to 1.1 */
		taper=(a-1)/driven_data[1][X];
		parasitic_data[1][LENGTH]=a*driven_data[1][3]; /* reflector = a*driven */
		for(i=2; i<=parasites; ++i)
				parasitic_data[i][LENGTH]=parasitic_data[1][3]-taper*(parasitic_data[i][X]-driven_data[1][X]);
	}

	if ((randomisation_method & RESONATE_DRIVEN)==RESONATE_DRIVEN) /* 128 */
	{
		if(run_first_time==1)
		{
			elements=driven+parasites;
			z=dmatrix(1L,(long)elements,2L,2L*(long)elements);
			l=0.44*lambda;
			do
			{
				driven_data[1][LENGTH]=l;			
				self_impedance(1, frequency*1e6,driven,parasites,driven_data,z);
				l+=0.00005*lambda;
				driven_data[1][LENGTH]=l;			
				x=z[1][2];
			}while(z[1][2]<0.0);
			printf("Driven element set to :l=%f m = %f wavlengths\n",l,l/lambda);
			free_dmatrix(z,1L,elements,1L,2L*elements);
		}
	}
#ifdef DEBUG
	if(errno)
	{
	fprintf(stderr,"Errno =%d in randomis.c\n", errno);
	exit(1);
	}
#endif
}

