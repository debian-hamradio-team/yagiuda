#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include "yagi.h"

#define MAX 1000

void         seedRNG();
int main(int argc, char **argv)
{
	int i, k;
	double realtype=0, inttype=0;

	k=MAX;

	seedRNG();

	printf("This program prints a few integers and reals, to test your implementation of the random number generator (RNG) is okay. Run the program again to check they produce a different set of numbers - if not the RNG is not seeded correctly.\n");
	for(i=1;i<=10;++i)
		printf("%f %d\n", randreal(), randint());
	printf("Now we print the average of %d floats and integers. If the figures are wildly different from those expected, check the RNG. The integer type must retun a number between 0 and 2^15-1. If It returns one between 0 and 2^31-1, you will need to modify the function randint()\n",k);
	for(i=1;i<=MAX;++i)
	{
		realtype+=randreal();
		inttype+=randint();
	}
	printf("The mean of %d floats was %.4f. It should have been about 0.5\n", MAX, realtype/MAX);
	printf("The mean of %d integers was %.4f. It should have been about 16384\n", MAX, inttype/MAX);
	exit(0);
}
