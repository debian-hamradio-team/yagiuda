#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include "yagi.h"
extern int errno;
extern double Zo;

void reflection_coefficient(struct FCOMPLEX zi, double *magnitude, double *phase)
{
	struct FCOMPLEX zo, rho;

	/* reflection coefficient rho = (Zi-Zo)/(Zi+Zo) */
	zo.r=Zo;
	zo.i=0;
	rho=Cdiv(Csub(zi, zo), Cadd(zi,zo));
	*magnitude=Cabs(rho);
	*phase=atan2(rho.i,rho.r);
#ifdef DEBUG
	if(errno)
	{
	fprintf(stderr,"Errno =%d in refco.c \n", errno);
	exit(1);
	}
#endif
}
