#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include "yagi.h"
extern int errno;

/* This function write the x and y locations of the centre of the element and
the length of the elements to disk */

void write_coordinates_of_elements_to_disk(FILE *ofp, int driven,
int parasitic, double **d, double **p)
{
	int element_number, before;
	before=ftell(ofp);
	for(element_number=1;element_number<=driven;++element_number)   
		fwrite((char *) &d[element_number][1], sizeof(double),3, ofp);  
	for(element_number=1;element_number<=parasitic;++element_number)        
		fwrite((char *) &p[element_number][1], sizeof(double),3, ofp);

#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in write_co.c\n", errno);
		exit(1);
	}
#endif
}

