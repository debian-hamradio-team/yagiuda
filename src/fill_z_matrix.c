#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <errno.h>
#include "yagi.h"
extern int errno;
/* We calculate the full Z matrix. Since the matrix is symmetrical, we only
need compute Z_i,j for j=1 to i. The function mutual, filles in Zji, when
we ask it to fill in Zij. */

void fill_z_matrix(double frequency, int driven, int parasitic, double **d, double **p, double **impedance)
{

	int i,j, elements=driven+parasitic;
	for(i=1;i<=elements;++i)			/* for every antenna element */
	{
		for(j=1;j<=i;++j)     
		{
			if(i==j)							 				/* Need self impedance */
			{
				if(i<=driven)
				{
					self_impedance(i, frequency, driven, parasitic, d, impedance);
				}
				else if (i > driven ) 					  /* parasitic element */
				{
					self_impedance(i, frequency, driven, parasitic, p, impedance);
				}
			}
			else if (i != j)		/* need mutual impedance */
			{
				 mutual_impedance(i,j,frequency,driven,parasitic, d, p, impedance); 
			}
		}
	}

#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in z.c\n", errno);
		exit(1);
	}
#endif
}
