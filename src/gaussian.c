#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include "yagi.h"

extern int errno;

/* The function Gaussian (normal) distibuted  deviate with 0 mean
and unit standard deviation. This is used in yagi to as it best represents
the way mechanically the antenna elemnts will be placed */

double gaussian()
{
	double fac,r,x,y;

		do {
			x=2.0*randreal()-1.0;
			y=2.0*randreal()-1.0;
			r=x*x+y*y;
		} while (r >= 1.0 || r==0.0);
		fac=sqrt(-2.0*log(r)/r);

#ifdef DEBUG
	if(errno)
	{
	fprintf(stderr,"Errno =%d in gaaussian.c\n", errno);
	exit(1);
	}
#endif
		return y*fac;
}
