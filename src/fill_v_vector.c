#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include "yagi.h"
extern int errno;

void fill_v_vector(int driven, int parasitic, double **driven_data, double *v)
{
	double real, imaginary;
	int i, elements=driven+parasitic;

	for(i=1;i<=elements;i++)		/* fill driven elements */
	{
		if(i<=driven)
		{
			real      = driven_data[i][VOLTAGE_R];
			imaginary = driven_data[i][VOLTAGE_I];
			v[2*i-1]=real;
			v[2*i]=imaginary;
		}
		else  /* zero parasitic elements */
		{
			v[2*i-1]=0.0;
			v[2*i]=0.0;
		}
	}

#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in fill_v_vector() of v.c\n", errno);
		exit(1);
	}
#endif
}
