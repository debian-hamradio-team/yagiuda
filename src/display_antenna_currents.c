#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include "yagi.h"

void display_antenna_currents(struct FCOMPLEX *current, int elements)
{
	int j, stars, k;
	double magnitude, max=0.0;
	for(j=1;j<=elements;++j)
	{
		magnitude=sqrt(current[j].r*current[j].r+current[j].i*current[j].i);
		if (magnitude > max)
			max=magnitude;
	}
	/* find the reflector current */
	magnitude=sqrt(current[2].r*current[2].r+current[2].i*current[2].i);
	stars=(int) (68.0*magnitude/max+0.5);
	printf("REF ");
	for(k=1;k<=stars;++k)
			printf("*");
	for(k=k;k<=68;k++)
		printf(" ");
	printf(" %.3f\n",magnitude/max);

	printf ("DR  ");
	magnitude=sqrt(current[1].r*current[1].r+current[1].i*current[1].i);
	stars=(int) (68.0*magnitude/max+0.5);
	for(k=1;k<=stars;++k)
			printf("*");
	for(k=k;k<=68;k++)
		printf(" ");
	printf(" %.3f\n",magnitude/max);
	for(j=3;j<=elements;++j)
	{
		magnitude=sqrt(current[j].r*current[j].r+current[j].i*current[j].i);
		stars=(int) (68.0*magnitude/max+0.5);
		printf("D%02d ",j-2);
		for(k=1;k<=stars;++k)
			printf("*");
		for(k=k;k<=68;k++)
			printf(" ");
		printf(" %.3f\n",magnitude/max);
	}
}
