extern char     *optarg;
extern int optind, opterr;
extern int errno;

double original_percent=10; /* Automatic changing: 10, to 1, then 0.1 then 0.01% */
double percent;
double magnitude, phase;
double Zo=Z0; /* Z0 is defined in yagi.h, Zo can be set in optimise */
struct performance_data weight,max;
double max_gain=1, boom_factor=1000, diameter, best_perf;
int popsize=0;
int iterations, fitness_method=0;
double vswr=1.0;
double boom_sd, length_sd;
int errno;
int K_times=1, K_times_max=10;
