#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include "yagi.h"

/* The function lew_length, computes the length of a dipole to have the same
reactance as an old dipole */

double new_length(double old_l, double old_dia, double lambda, double new_dia)
{
	double old_r, old_x, r, x,l;
	self(old_dia/2.0, old_l, lambda, &old_r, &old_x);
	/* printf("old r=%f old l=%f old x=%f\n",old_r,old_l,old_x); */
	if(old_dia > new_dia)
		l=0.50001*lambda;	
	else
		l=old_l;
	do{ 
		l-=0.1*lambda;
		self(new_dia/2.0, l, lambda, &r, &x);
	}while(x>old_x);
	l+=0.1*lambda;
	do{ 
		l-=0.01*lambda;
		self(new_dia/2.0, l, lambda, &r, &x);
	}while(x>old_x);
	l+=0.01*lambda;
	do{ 
		l-=0.001*lambda;
		self(new_dia/2.0, l, lambda, &r, &x);
	}while(x>old_x);
	l+=0.001*lambda;
	do{ 
		l-=0.0001*lambda;
		self(new_dia/2.0, l, lambda, &r, &x);
	}while(x>old_x);
	l+=0.0001*lambda;
	do{ 
		l-=0.00001*lambda;
		self(new_dia/2.0, l, lambda, &r, &x);
	}while(x>old_x);
	l+=0.00001*lambda;
	do{ 
		l-=0.00001*lambda;
		self(new_dia/2.0, l, lambda, &r, &x);
	}while(x>old_x);
	/* printf("new r=%f new l=%f new x=%f\n",r,l,x); */
	return(l);
}
