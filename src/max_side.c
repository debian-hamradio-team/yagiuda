#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include "nrutil.h"
#include "yagi.h"

/* This routines finds the maximum sidelobe level in the antenna pattern, 
outside the main beam. The fasteest one, find_max_sidelobe_fast is used by
optimise when seeing how clean the pattern is. Its not perfect, but its
fast. 

The other one, find_max_sidelobe_slow is much more accurate, doing a good
job of finding the sidelobes, but takes longer to do. Hence its not
really suitable for optimisation, but its okay for 'output' where we dont
care too much about spped. */

extern double angular_stepsize_2,min_offset_from_peak;

#define BETTER 1
#define WORSE 2

#define STEP  1   /* anglur step in degrees */

double dB_down_from_peak(double x, double pin, struct  element_data *coordinates, struct FCOMPLEX *current,int elements, double f, double design_f)
{
		double ans,gain_H_plane,peak_gain, gain_at_x;
		gain(90.0, 0.0, pin, f/design_f, coordinates, current, elements, &peak_gain, &gain_H_plane, f, design_f);
		gain(x, 0.000, pin, f/design_f, coordinates, current, elements, &gain_at_x, &gain_H_plane, f, design_f);
		ans=peak_gain-gain_at_x;
		return(ans);
}


double find_max_sidelobe_slow(double gain, double pin,struct element_data *coordinates, struct FCOMPLEX *current, int elements, double frequency,double design_f)
{
	double level,best=-1000.0,a=90.0,theta,max=1e8;
	int k;
	do{
		a+=STEP; /* Use 1 degree or similar step*/
		level=dB_down_from_peak(a,pin,coordinates,current,elements,frequency,design_f);
		if(level>best)
		{
			k=BETTER;
			best=level;
		}
		else
			k=WORSE;
		}while(k==BETTER && a<=270.0);
	a-=STEP;
	for(theta=a; theta<270.01; theta+=STEP)
	{
		level=dB_down_from_peak(theta,pin,coordinates,current,elements,frequency,design_f);
		if(level<max)
		{
			max=level;
			a=theta;
		}
	}
	return(max);
}


double find_max_sidelobe_fast(double gain, double pin,struct element_data *coordinates, struct FCOMPLEX *current, int elements, double frequency,double design_f)
{
	double angle=90.0,min_angle,min,max=270,three_dB_point,min_level=1000000.0;
	double level, min_times;
	int N;

	if(min_offset_from_peak==0.0)
	{
		three_dB_point=sqrt(41000.0/(pow(10.0,gain/10.0))); 
		if(three_dB_point > 90.0)
			min=180.0;
		else
			min=90.0+(three_dB_point);
	}
	else
		min=90.0+min_offset_from_peak;
	if(angular_stepsize_2==0.0)
		angular_stepsize_2=three_dB_point/30.0; /* step sixe to use - rough */
		/* Since this is only an approximate quick method, you cant expect 
		supeurb accuracy - use _fast if you want that. I'll evaluate at 
		1/10th of the beamwidth, so if the 3dB beamwidth is estimated to be
		40 deg, we will evualate every 4 deg. However, we need to evalate at
		precise 270 degrees, otherwise its possible the max sidelobe will be 
		less than the FB, which is silly. To avoid this, I'll ensure its   
		evaluated at 270 deg, so the step size will be altered to do this. */
   min_times=(max-min)/angular_stepsize_2;
	N=(int) (min_times+1.0);

	for(angle=min; angle <=max;angle+=(max-min)/N)
	{
		level=dB_down_from_peak(angle,pin,coordinates,current,elements,frequency,design_f);
		if(level<min_level)
		{
			min_level=level;
			min_angle=angle;
		}
	}
	/* printf("min=%.2f min_angle=%.2f angle=%.2f f=%f\n ",min_level,min_angle,angle,frequency);  */
	return(min_level);
}



			
