#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include "yagi.h"

void print_z_matrix(double frequency, int elements, double **z)
{
	int i,j;		
	printf("Z - Matrix at frequency = %f\n", frequency);
	for(i=1;i<=elements;++i)
	{
		for(j=1;j<=2*elements;++j)
			printf("% 7.2f ", z[i][j]);
		printf("\n");
	}  
}
