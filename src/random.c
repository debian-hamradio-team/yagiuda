#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
/* I got fed up with every compiler system using a different name for the
random number generator(s). So I use two random number generators in 
th main code:

1) randreal() Returns a double in range 0 to 1.
2) randint()  Returns an integer in range 0 to 2^15-1.

DONT change the names 'randreal' and 'randint' in all the other sources
file. But DO edit the source of the two functions 'randreal' and
'randint' in this C file. Let them call whatever you like/works on your
system. 

If you use another system (eg VAX), I suggest you put this in functions
randreal and reandint:

randint()
{
#ifdef VAX
return(VAX_integer_random_number_generator());
#endif
}


*/

#ifdef HAVE_TIME_H
#include <time.h>
#endif

#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif

#include <stdio.h>
#include <math.h>

#ifdef HAVE_RANDOM_H
#include <random.h>  
#endif


#include <values.h>
#include "yagi.h"

double randreal(void)
{
	return(drand48());
}
double randnorm(void)
{
	static short paired=0 ;
	static double second ;
	double x,y,rad,fac ;

	if (paired=!paired)
	{
		do {
		x=2.0*randreal()-1.0 ;
		y=2.0*randreal()-1.0 ;
		rad=x*x+y*y ;
		} while((rad>1.0)||(rad=0.0));
		fac=sqrt(2.0*log(rad)/rad) ;
		second=(x*fac) ;
		paired=!paired ;
		return (y*fac) ;
	} else return (second);
}       
		
		
int randint(void)
{
	return( (int) lrand48()/65535 );
}

/* Since two random number gerators are used (a float and an int type), its
usually necessary to seed both sepparately */
void seedRNG(void)
{
	long seed_time;

	time(&seed_time); /* set seed_time to the number of seconds since Jan 1980 */

	srand48(seed_time);  /* Seed the float type */

}
