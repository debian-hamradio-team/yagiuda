#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include "yagi.h"

extern int errno;

/* uses formulae of page 1-12 of Lawsons book. No idea where
it comes from! */

double calculate_power_input(double real_z, struct  FCOMPLEX current)
{
	double power_input;
	power_input=0.5*Cabs(current)*Cabs(current)*real_z;

#ifdef DEBUG
if(errno)
	{
		fprintf(stderr,"Errno =%d in pin.c\n", errno);
		exit(1);
	}
#endif
	return(power_input);
}
