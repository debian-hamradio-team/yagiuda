#include <stdio.h>
#include "nrutil.h"
#include "com_hack.h"
#include "nr_hack.h"

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define LIN 0 
#define LOG 1

#define MAX_LINE  100  /* Maximum characters on one line of input file */

#define DRIVEN_LENGTH      1 /* What to move - see randomis.c */
#define DRIVEN_POSITION    2
#define REFLECTOR_LENGTH   4
#define DIRECTOR_LENGTH    8
#define DIRECTOR_POSITION 16
#define ALL_ELEMENT_LENGTHS_IDENTICAL 32
#define LINEAR_TAPER                  64
#define RESONATE_DRIVEN              128

/* better criteria, see optimise and better.c */

#define GAIN                  1
#define FB                    2
#define RESISTANCE            4
#define REACTANCE             8
#define VSWR                 16
#define SIDE_LOBE_LEVEL      32
#define REASONABLE        32768 

#define REASONABLE_G
#define REASONABLE_FB       27
#define REASONABLE_R         5
#define REASONABLE_X         5
#define REASONABLE_SWR       1.1
#define REASONABLE_SIDELOBE 23

#ifndef Z0
#define Z0                             50
#endif
#define E_MAX 179.0
#define H_MAX 60.0

#define RRANGE_MIN -50.0
#define RRANGE_MAX  20.0

#define EULER 0.57721566

#define MAX_DRIVEN                 6 /* There are 6 bits info on a driven ele */
#define MAX_PARASITIC              4 /* But only 4 on a parasitic */

#define  X 			                 1/* index into driven and parasitic arrays */
#define  Y 			                 2
#define  LENGTH	                 3
#define	DIAMETER	                 4
#define	VOLTAGE_R                 5/* Volt and phase not on parasistic ele */
#define	VOLTAGE_I                 6
#define 	REAL			              0
#define 	IMAGINARY	              1
#define  HEADER_SIZE             100/* size of header in .out files in bytes */   

typedef struct element_data {double x,y,length;} fred;

typedef struct performance_data {double r,x,gain,fb,swr,sidelobe;} f4;

typedef struct flags {int aflg, bflg, cflg, dflg, eflg, errflg,fflg;
		int gflg, hflg, iflg, jflg, kflg, lflg, mflg, nflg, oflg, pflg;
		int qflg, rflg, sflg, tflg, uflg, vflg, wflg, xflg, yflg, zflg;
		int Aflg, Bflg, Cflg, Dflg, Eflg, Fflg, Gflg, Hflg, Iflg, Jflg;
		int Kflg, Lflg, Mflg, Nflg, Oflg, Pflg, Qflg, Rflg, Sflg, Tflg;
		int Uflg, Vflg, Wflg, Xflg, Yflg, Zoflg;} f2;

typedef struct pattern {
			double three_dB_E, three_dB_H;
			double first_null_E, first_null_level_E;
			double first_null_H, first_null_level_H;
			double first_sidelobe_E, first_sidelobe_level_E;
			double first_sidelobe_H, first_sidelobe_level_H;
								}f3;
int main(int argc, char **argv);
int linear_current_optimisation_test(struct FCOMPLEX *cur, double *old_sd, int elements, int parasites, struct flags flag);
void display_currents( struct FCOMPLEX *currents, int elements);
void dynamic_changing_of_weights(int i, int div, struct performance_data *weight);
double Cin(double x);
double ci(double x);
double new_length(double oldl,double old_dia, double lambda, double new_dia);
void end_if_stop_exists(int *i, int iterations,int divisor);
struct performance_data subtract_structures(struct performance_data a, struct performance_data b);
void print_relavent_performance_data(FILE *fp, char *s, int i, struct flags flag, struct performance_data data,double fitness, int Z_Q, int fitness_Q);
/* char *s, int i, structure flags flag, \
structure performance_data data); */
void set_mean_structure(struct FCOMPLEX input_impedance,double E_fwd, double E_back,struct flags flag, double pin,struct element_data *coordinates, struct FCOMPLEX *current, int elements, double f, double design_f, struct performance_data *mean);
void test_for_stop_file(void);
void set_performance_structures(struct performance_data *weight, struct performance_data *max, struct performance_data *best, struct performance_data *worst);
void optimising_for(struct flags flag);
double dB_down_from_peak(double x, double pin, struct  element_data *coordinates, struct FCOMPLEX *current,int elements, double f,double design_f);
double find_max_sidelobe_fast(double gain, double pin,struct element_data *coordinates, struct FCOMPLEX *current, int elements, double frequency,double design_f);
double find_max_sidelobe_slow(double gain, double pin,struct element_data *coordinates, struct FCOMPLEX *current, int elements, double frequency,double design_f);
void copy_matrix(int length, int width, double **to, double **from);
char *get_data_filenames(int argc, char **argv, char *input);
char *string(long lower, long upper);
void free_string(char *string, long lower, long upper);
int get_number_of_elements(char *datafile, int *driven, int *parasitic);
void read_yagi_data(char *line, char *file, double *frequency, double *min_frequency, double*max_frequency, double *step_frequency, int dr, double **dr_data, int parasitic, double **para_data, double *angular_step);
void   self_impedance(int i, double frequency, int driven, int parasitic, double **data, double **impedance);
void self2(double r, double l, double wavel, double *rin, double *xin);
void   mutual_impedance(int i, int j, double frequency, int driven, int parasitic, double **d, double **p, double **impedance);
void   fill_z_matrix(double frequency, int driven, int parasitic, double **d, double **p, double **impedance);
void z21(double lamda, double d, double mean_length, double *r21, double *x21);
double ci(double); 
void fill_v_vector(int driven, int parasitic, double **d, double *v_vector);
void write_header_to_disk(FILE *ofp, int elements, int driven,
int parasitic, double min_frequency, double max_frequency, double frequency,
double step_frequency, double angular_step);
void write_vector(double *v, int elements, FILE *ofp);
int  read_header(FILE *ifp, FILE *ofp, double *min_f, double *max_f, double *step_f, double *f, double *angular_step);
void z_input(struct FCOMPLEX v, struct FCOMPLEX i, struct FCOMPLEX *z);
void reflection_coefficient(struct FCOMPLEX z_input, double *mag, double *phase);
double calculate_vswr(double magnitude);
double calculate_power_input(double real_z, struct FCOMPLEX current);
void   write_coordinates_of_elements_to_disk(FILE *ofp, int driven,
int parasitic, double **driven_data, double **parasitic_data);
void gain(double theta, double phi, double  pin, double normallised_f, struct
element_data *cordinates,struct FCOMPLEX *current, int elements, double *E, double *H, double actual_frequency, double design_frequency);
struct FCOMPLEX E_to_complex_power(struct FCOMPLEX x);
struct FCOMPLEX **FCOMPLEXmatrix(long a, long b, long c, long d);
void free_FCOMPLEXmatrix(struct FCOMPLEX **m, long nrl, long nrh, long ncl, long nch);
void free_FCOMPLEXvector(struct FCOMPLEX *v, long nrl, long nrh);
void free_element_data_vector( struct element_data *v, long nl, long nh);
double change_max_percentage_changes(int i, int iterations, double orig_pc);
struct FCOMPLEX *FCOMPLEXvector(long a, long b);
struct element_data *element_data_vector(long low, long high);
void print_z_matrix(double f, int elements, double **z_matrix);
void write_gain_at_various_angles(FILE *gain_fp, double angular_step, double pin, double normalised_f, double f, struct element_data *coordinates, struct FCOMPLEX *current, int elements, double design_f);

void write_input_data_to_disk(FILE *fp, char *notes, double frequency, double min_f, double max_f, double step_f, int elements ,int driven,int parasitic, double angular_step,double **d, double **p, double scale_factor); 
void randomise(int randomisation_method, double frequency, double percent, double **driven_data, double **parasitic_data, int driven, int parasitic);
void automatic_enhancement(struct flags flag, double frequency, double **driven_data, double **parasitic_data, int driven, int parasitic, struct FCOMPLEX *voltage, struct FCOMPLEX *current, struct FCOMPLEX *input_Z, double *v, double **z, double **A, double *b, int *indx, struct element_data *coordinates);

/* int is_it_better(int criteria,struct performance_data new,struct performance_data best);  */
int is_it_better(int criteria, struct performance_data n, struct performance_data b);
double version(void);
void get_command_line_options(int argc, char **argv, struct flags *f);
void write_data_for_gnuplot(FILE *fp1,FILE  *fp2,FILE *fp3,FILE *fp4,FILE *fp5,FILE *fp6, double f,double normalised_f,double input_impedance_r, double input_impedance_i, double vswr, double gain_E_plane,double  fb_ratio);
void usage_first(char *str);
void usage_input(char *str);
void usage_yagi(char *str);
void usage_output(char *str);
void usage_optimise(char *str);
void write_file_for_gnuplot_to_load(FILE *fp, char *filename);

double performance(struct flags flag, struct performance_data data,struct performance_data weights, struct performance_data max, struct performance_data start);
int getoptions(int argc, char **argv, char *opts);

void copy_complex_data_to_real_matrix(int elements, double **from, double **to);
void show_all_first_parameters(char *exefile);
void show_all_optimise_parameters(char *exefile,struct flags flag);
double randreal(void);
int    randint(void);
void solve_equations(double frequency, int driven, int parasitic, double **driven_data, double **parasitic_data, double *v, double **z, double *pin, struct FCOMPLEX *voltage, struct FCOMPLEX *current, struct FCOMPLEX *input_impedance, struct element_data *coordinates, double **A, double *b, int *indx);
void genetic_algorithm(char *output_filename, char *update_filename, struct flags flag, double frequency, double minf, double maxf, double stepf, double angular_step, int driven ,int  parasitic, double **driven_data, double **parasitic_data, double *v, double **zz, double
*pin, struct FCOMPLEX *voltage, struct FCOMPLEX *current, struct FCOMPLEX
*input_impedance, struct element_data *coordinates, double **A, double *b, int
*indx,struct performance_data *mean_performance);

double get_genetic_algorithm_fitness(struct flags flag, double frequency, int driven, int parasitic, double **driven_data, double **parasitic_data, double *v, double **z, double *pin, struct FCOMPLEX *voltage, struct FCOMPLEX *current, struct FCOMPLEX *input_impedance, struct element_data *coordinates, double **A, double  *b, int *indx, struct performance_data *data2);

void check_flags(struct flags flag, int argc, int optind, char *exefilename);
void error_message(char *str);
double determine_maximum_gain(double f, double  l);
double determine_maximum_gain2(int elements);


void do_since_better(int i, char *output_filename, char *update_filename, struct FCOMPLEX input_impedance, struct performance_data n,struct flags flag,char * notes,double frequency, double min_frequency,double max_frequency,double  step_frequency,int elements, int driven,int parasitic,double angular_step,double **driven_data,double **parasitic_data,double scale_factor,double new_performance);
double gaussian();
void sensitivity(double boom_sd, double length_sd, double **driven_data, double**parasitic_data, int driven, int parasites);
double log2(double x);
void mprove(double **a ,double **LU_of_a,int n, int *indx, double *b, double *x);
void seedRNG(void);
double error_3dB_E(double x); 
double error_3dB_H(double x); 
double zbrent(double (*func)(), double x1, double x2, double tol);  
double Objective(char *gene);
int GA_Free(void);
int GA_Error(char *error_mesg);
void SetPrint(int a);
int Initialise(int popsise, int genesize);
int Selection(FILE *fd, int gene);
double ss2r(char *string, int pos, int len);
void write_gnuplot_header(FILE *fp, double f, char *filename, int step, int lin_or_log);
void self(double r, double length, double lambda, double *Rin, double *Xin);
void display_antenna_currents(struct FCOMPLEX *current, int elements);
