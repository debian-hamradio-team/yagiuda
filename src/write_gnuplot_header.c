#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include "yagi.h"

/* These range variables select the graph range and are defined in output.c */

extern double xrange_min, xrange_max, yrange_min, yrange_max, rrange_min, rrange_max;

void write_gnuplot_header(FILE *fp, double f, char *filename, int step, int lin_or_log)
{
	fprintf(fp,"#\n");
	fprintf(fp,"# $Id: %s,v 1.4 1993/09/27 17:10:59 alex Exp $\n",filename);
	fprintf(fp,"#\n");
	fprintf(fp,"#\n");
	fprintf(fp,"# Analsed from %s with Yagi-Uda \n", filename);
	fprintf(fp,"set angles degrees\n"); 
	fprintf(fp,"set size square\n");
	fprintf(fp,"set polar\n");
	fprintf(fp,"set grid polar 15.\n");
	fprintf(fp,"set noborder\n");
	fprintf(fp,"set noparam\n");
	fprintf(fp,"set lines\n");
	if(lin_or_log == LIN)
	{

	} 
	else if (lin_or_log==LOG)
	{
		fprintf(fp,"set xrange [%f:%f]\n",-(rrange_max-rrange_min), rrange_max-rrange_min);
		fprintf(fp,"set yrange [%f:%f]\n",-(rrange_max-rrange_min), rrange_max-rrange_min);
		fprintf(fp,"set rrange [%f:%f]\n",rrange_min, rrange_max);
		fprintf(fp,"set trange [-pi:pi]\n");
	}
	fprintf(fp,"set xtics 0, 10, %f\n",-rrange_min+rrange_max);
	fprintf(fp,"set ytics 0, 10, %f\n",-rrange_min+rrange_max);
	fprintf(fp,"set xtics axis mirror\n");
	fprintf(fp,"set ytics axis mirror\n");
	fprintf(fp,"set data style line\n");
}

