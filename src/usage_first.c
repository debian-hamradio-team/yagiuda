#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include "yagi.h"

void usage_first(char *exefile) 
{
	fprintf(stderr,"\nYagi-Uda antenna analysis programs, version %.2f\n", version());
	fprintf(stderr,"Written by Dr. David Kirkby Ph.D. G8WRB (email:david.kirkby@onetel.net)\n");
	fprintf(stderr,"Usage: %s filename elements min_f f max_f step_f diameter\n",exefile);
	fprintf(stderr,"       where frequencies are in MHz and diameter is in mm\n");
	fprintf(stderr,"'first' is used to define an antenna quickly, according to DL6WU designs.\n");
	fprintf(stderr,"It is much faster in use than 'input'\n");
		fprintf(stderr,"Example: first 145e20 20 144 145 146 .1 6.3\n\n");
		fprintf(stderr,"will make a file '145e20' containing a description of a 20 element DL6WU yagi, \nfor 144-146 MHz, designed at 145MHz, which 'yagi' will calculate in steps of \n0.1MHz. The antenna uses 6.3mm diameter elements.\n");

}
