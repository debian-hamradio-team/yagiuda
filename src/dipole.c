#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include "yagi.h"

int main(int argc, char **argv)
{
   double freq, r, x, real, length, lambda, dia;
   if (argc != 4) {
     fprintf(stderr,"Usage: frequency(MHz) length(m) diamater(mm)\n");
     exit(1);
     }
   freq=atof(argv[1]);
   length=atof(argv[2]);
   dia=atof(argv[3]); 

   r=dia/2000;
   lambda=300/freq;

   self(r, length, lambda, &real, &x);
   printf("Self impedance of a dipole:\n");
   printf("%f MHz,  length %f m, diameter %f mm, is \n",freq,length,dia);
   printf("Z = %f  %+f jX Ohms\n", real,x);
   exit(0);
}
