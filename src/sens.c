#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
/* file sens.c */

#include <stdio.h>
#include <math.h>

#include <errno.h>
#include "yagi.h"

extern int errno;
extern double boom_factor;

void sensitivity(double boom_sd, double length_sd, double **driven_data, double**parasitic_data, int driven, int parasites)
{
	int i;
	double x,y;
	/* data in 2D arrays is in m. User enters SD in mm, hence *0.001 */

	
	/* adjust director length only. Keep at x=0 */

	x=parasitic_data[1][LENGTH];
	parasitic_data[1][LENGTH]+=length_sd*0.001*gaussian();
	y=parasitic_data[1][LENGTH];
	/* Adjust other parasites */
	for(i=2;i<=parasites;++i)
	{
		parasitic_data[i][X]+=boom_sd*0.001*gaussian();
		parasitic_data[i][LENGTH]+=length_sd*0.001*gaussian();
	}
	for(i=1; i<=driven; ++i)
	{
		driven_data[i][X]+=boom_sd*0.001*gaussian();
		driven_data[i][LENGTH]+=length_sd*0.001*gaussian();
	}

#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in sens.c\n", errno);
		exit(1);
	}
#endif

}

