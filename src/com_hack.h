/* Here are the prototypes of the complex routines that are in the book
'Numerical recipes in C' by Press et al, 2nd edition, 1992. All references to 
'float' in that book have been changed to 'double' here. Hence the definitions 
are not always the same.

ie a routine:
				 fcomplex complex(float re, float im);

has been changed to:

             fcomplex complex(double re, double im);

Hence all the C routines used here *must* be converted to double. Please *dont*
use the numerical recipes header file 'complex.h', use my 'complex_hack.h' 

The line:

typedef struct FCOMPLEX {double r, i;} fcomplex;

in the file  complex_hack.c must be deleted, since its defined in this file.
Otherwise its defined twice, which will give a compiler error. */

typedef struct FCOMPLEX {double r, i;} fcomplex;

fcomplex Cadd(fcomplex a, fcomplex b);  /* a+b */
fcomplex Csub(fcomplex a, fcomplex b);  /* a-b */
fcomplex Cmul(fcomplex a, fcomplex b);  /* a*b */
fcomplex Complex(double re, double im); /* Set an fcomplex number */
fcomplex Cdiv(fcomplex a, fcomplex b);  /* a/b */
double Cabs(fcomplex z);                /* absolute value of z */

fcomplex RCmul(double x, fcomplex a);   /* real (x) times compelx (a) */

