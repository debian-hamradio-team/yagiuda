#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <string.h>
#include "yagi.h"

extern double percent;
extern double  Zo;
extern struct performance_data max;

void usage_optimise(char *str)
	{

		fprintf(stderr,"\nYagi-Uda antenna analysis programs, version %.2f\n", version());
		fprintf(stderr,"Written by Dr. David Kirkby Ph.D. G8WRB (email:david.kirkby@onetel.net)\n");
		fprintf(stderr, "\nUSAGE: %s  [options] filename iterations.\n\n", str);
		fprintf(stderr,"The default behaviour of 'optimise' is to optimise the Yagi description \ncontained in the file 'filename', adjusting the lengths and positions of just \nthe parasitic elements upto %.2f%% each time, to achieve a better gain, vswr and F/B ratio, but not bothering to go beyond reasonable limits currently set at:\n",percent);
		fprintf(stderr,"  F/B ratio > %.1f dB\n", max.fb);
		fprintf(stderr,"  %f < Rin < %f \n", Zo-max.r, Zo+max.r);
		fprintf(stderr,"  | Xin | < %.2f Ohms\n", max.x);
		fprintf(stderr,"  VSWR < %.2f:1\n", max.swr);
		fprintf(stderr,"This will continue for the number of iterations specified in 'iterations' or \nuntil a file called 'stop' is created in the current directory.\n");
		fprintf(stderr,"These are a number of options for 'optimise' which can adjust such things as:\n");
		fprintf(stderr," -gx=GA, -ox = improve all, -Wx = weighted\n");
		fprintf(stderr," -gx or ox or -Wx where x=1(G),2(FB),4(R),8(X),16(VSWR),32 (SL)\n"); 
		fprintf(stderr," -f, -p, -r, -s, -x Set reasonable values of F/B, pattern cleanliness, R, VSWR and X\n");
		fprintf(stderr," -F, -G, P, -R -S and -X Set weights of F/B, gain , pattern cleanliness, resistance, VSWR and reactance, (all need -W or -g flags)\n");
		fprintf(stderr,"There are many many more options. See man page, optimise.doc, or optimise.ps. Sensible use of these options will give you a decent Yagi.\n"); 
		exit(1);
	}

