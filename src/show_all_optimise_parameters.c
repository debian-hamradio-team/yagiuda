#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <errno.h>
#include "yagi.h"
extern int errno;

/* Since there are a large number of parametres set in the header file
and 'optimise.c', I've added a function to print the lot. This will
be more accurate than putting them in the man page, since that tends
to get out of date */

extern double  percent;
extern double Zo, boom_factor;
extern struct performance_data max,weight;

void show_all_optimise_parameters(char *exefile,struct flags flag)
{
	int choice=flag.oflg;
	printf("%s version %.2f\n", exefile,version() );
	printf("Zo=%f Ohms \n", Zo);
	printf("Acceptable VSWR < %.3f:1\n", max.swr);
	printf("Acceptable FB > %.3f dB\n", max.fb);
	printf("Acceptable reactance < %.3f Ohms\n", max.x);
	printf("Acceptable resistance %.3f < R < %.3f Ohms\n", Zo-max.r, Zo+max.r);
	printf("Maximum percentage change between each iteration is %.6f%%\n", percent);
	printf("weight_gain=%.4f weight_fb=%.5f weight_swr=%.4f\n", weight.gain, weight.fb, weight.swr);
	printf("Type of elements moved/adjusted is %d\n", flag.eflg);
	printf("Default choice of optimisation method is %d\n", choice);
	printf("Boom will not extend by more than %.3f%%\n", boom_factor);

#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in showopt.c\n", errno);
		exit(1);
	}
#endif
}

