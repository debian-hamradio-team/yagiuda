#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>

#include <errno.h>
#include "yagi.h"
extern int errno;


/* We calculate the vswr, given the magnitude of the reflection 
coefficient */ 
double calculate_vswr(double magnitude)
{
	double swr;
	if(magnitude > 0.98 )
		magnitude=0.98;
	swr=(1+magnitude)/(1-magnitude);
#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in vswr.c\n", errno);
		exit(1);
	}
#endif
	return(swr);
}
