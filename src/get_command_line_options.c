#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include "yagi.h"

extern char     *optarg;
extern int optind, opterr;
extern int errno;
extern struct performance_data max, weight;
extern double original_percent; 
extern double percent;
extern double magnitude, phase;
extern double Zo; /* Z0 is defined in yagi.h, Zo can be set in optimise */
extern double max_gain, boom_factor, diameter, best_perf;
extern int popsize;
extern int iterations, fitness_method;
extern double vswr;
extern double boom_sd, length_sd;
extern int K_times, K_times_max;
int errno;
double min_offset_from_peak=0.0;
double angular_stepsize_2=0.0;

void get_command_line_options(int argc, char **argv, struct flags *flag)
{
	int c;
	/* Since not all complilers come with the 'getopts' functions, I've
	get a source of it and put it in the distribution of Yagi-Uda. 
	To avoid conflics on unix systems, I've rename it to getoptions */
   while((c=getoptions(argc,argv,"A:c:kwW:hvdg:Or:P:m:C:b:x:f:s:S:G:R:X:F:l:o:e:Z:p:t:T:K:")) != -1)
   switch       (c) 
   {
			 case 'k': /* To keep the old start point with the GA */
					flag ->kflg=1;
					break;
			 case 'C':     /* Optimise by making element current the same */
					flag -> Cflg=atoi(optarg);
					break;
			 case 'A': /* Automatically maximise */
					flag->Aflg=atoi(optarg)+1000;
					if(flag->Aflg <-1)
					{
						fprintf(stderr,"Aflg x, where (-1<= x<= directors)\n");
						exit(1);
					}
					break;
			 case 'W':  /* weighted inprovement */
					flag->Wflg=atoi(optarg);
					if(flag->Wflg <1 || flag->Wflg>64)
					{
						fprintf(stderr,"-Wx, where x=1(gain),2() etc AND\n");
						exit(1);
					}
					flag->Wflg+=32768;
			      break;
			 case 'a':  /* Angular step size to use when pattern searching */
					flag->aflg=1;
					if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -a which requires numeric data.\n");
						exit(1);
					}
					angular_stepsize_2=atof(optarg);
					if(angular_stepsize_2 < 0.0)
					{
						error_message("The '-a' requires a posistive float, signifying the angular stepsize to use when finding sidelobes.\n");
						exit(1);
					}
			      break;
			 case 'P':  /* Get average level of pattern down */
					flag->Pflg=1;
					if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -P which requires numeric data.\n");
						exit(1);
					}
					weight.sidelobe=atof(optarg);
					if(weight.sidelobe < 0.0)
					{
						error_message("The '-P' requires a posistive float, signifying the weight on the sidelobe level (default=1.0).\n");
						exit(1);
					}
			      break;
			 case 'c':  /* Get average level of pattern down */
					if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -P which requires numeric data.\n");
						exit(1);
					}
					max.sidelobe=atof(optarg);
					if(max.sidelobe < 0.0)
					{
						error_message("The '-P' requires a posistive float, signifying the sidelobe level down to aim for in dB (20 is reasonable).\n");
						exit(1);
					}
					break;
			 case 'm':  /* Get minimum deviation from theta=90, to condsider a sidelobe. Higher the gain antenna, the smaller it should be set. */
					flag->mflg=1;
					if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -m which requires numeric data.\n");
						exit(1);
					}
					min_offset_from_peak=atof(optarg);
					if(min_offset_from_peak < 0.0|| min_offset_from_peak > 90)
					{
						error_message("The '-m' requires a posistive float, signifying the minimum offset in degrees from theta =90, to start considering a sidelobe, rather than the main beam.\n");
						exit(1);
					}
			      break;
			 case 'w':  /* wide band ant wanted - avg at low, design and upper f */
					flag->wflg=2; /* do at three frequencies,  2 extra ones */
			      break;
			 case 'r':   /* acceptable_resistance_error option */
					flag->rflg=1;
					if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -r which requires numeric data.\n");
						exit(1);
					}
					max.r=atof(optarg);
					if(max.r < 0.0)
					{
						error_message("The '-r' option setting an acceptable vswr must >=0.0\n");
						exit(1);
					}
			      break;
			 case 'x':   /* acceptable_reactance option */
					flag->xflg=1;
					if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -x which requires numeric data.\n");
						exit(1);
					}
					max.x=atof(optarg);
					if(max.x < 0.0)
					{
						error_message("The '-x' option setting an acceptable reactance must >=0.0\n");
						exit(1);
					}
			      break;
			 case 's':   /* acceptable_vswr option */
					flag->sflg=1;
					if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -s which requires numeric data.\n");
						exit(1);
					}
					max.swr=atof(optarg);
					if(max.swr < 1.0)
					{
						error_message("The '-s' option setting an acceptable vswr must >= 1.\n");
						exit(1);
					}
			      break;
			 case 'f':   /* acceptable_fb_ratio option */
					flag->fflg=1;
					if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -f which requires numeric data.\n");
						exit(1);
					}
					max.fb=atof(optarg);
					if(max.fb < 0.0)
					{
						error_message("The '-f' option setting an acceptable FB ratio must >=0.0\n");
						exit(1);
					}
			      break;
			 case 'F':   /* fb_ratio weight */
					flag->Fflg=1;
					if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -F which requires numeric data.\n");
						exit(1);
					}
					weight.fb=atof(optarg);
					if(weight.fb < 0.0)
					{
						error_message("The '-F' option setting the weight for FB ratio must >=0.0\n");
						exit(1);
					}
			      break;
			 case 'G':   /* fb_ratio weight */
					flag->Gflg=1;
					if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -G which requires numeric data.\n");
						exit(1);
					}
					weight.gain=atof(optarg);
					if(weight.gain < 0.0)
					{
						error_message("The '-G' option setting the weight for gain must >=0.0\n");
						exit(1);
					}
			      break;
			 case 'S':   /* weight of swr*/
					flag->Sflg=1;
					if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -S which requires numeric data.\n");
						exit(1);
					}
					weight.swr=atof(optarg);
					if(weight.swr < 0.0)
					{
						error_message("The '-S' option setting the weight for swr must >=0.0\n");
						exit(1);
					}
			      break;
			 case 'l':   /* percentage change in ele positions */
					flag->lflg=1;
					if(!isdigit( (int) *optarg) && *optarg !='.'&& *optarg!='-')
					{
						error_message("Non numeric data entered for option -l which requires numeric data.\n");
						exit(1);
					}
					original_percent=atof(optarg);
			      break;
			 case 'o':   /* optimise for gain, fb etc etc */
					if(!isdigit( (int) *optarg))
					{
						error_message("Non numeric data entered for option -o which requires numeric data.\n");
						exit(1);
					}
					flag->oflg=atoi(optarg);
					if(flag->oflg< 0 || flag->oflg > 128)
					{
						error_message("The '-o' option setting the parameter(s) to optimise for must be in the range 0 to 128.\n");
						exit(1);
					}
			      break;
			 case 'O':
					flag->Oflg=1;
					break;
			 case 'K':   /* Keep to original data, until K bad goes */
					flag->Kflg=1;
					if(!isdigit( (int) *optarg))
					{
						error_message("Non numeric data entered for option -K which requires an integer.\n");
						exit(1);
					}
					K_times_max=atoi(optarg);
					if(K_times_max < 1 )
					{
						error_message("The '-K' option setting the number of attemps to stay with hte original data after a good one found, to avoid local optimums, must be an integer > 1.\n");
						exit(1);
					}
			      break;
			 case 'b':   /* how long can boom be extended */
					flag->bflg=1;
					if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -b which requires numeric data.\n");
						exit(1);
					}
					boom_factor=atof(optarg);
					if(boom_factor < 0.0)
					{
						error_message("The '-b' option setting the maximum permissable change in the boom length (in %%) must be >=0.0\n");
						exit(1);
					}
			      break;
			 case 'Z':   /* Characteristic impedance */
					flag->Zoflg=1;
					if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -Z which requires numeric data.\n");
						exit(1);
					}
					Zo=atof(optarg);
					if(Zo <= 0.0)
					{
						error_message("The '-Z' option setting Zo must be > 0.0\n");
						exit(1);
					}
			      break;
			 case 'e':   /* type of element moved (driven, parasitic or both) */
					if(!isdigit( (int) *optarg) )
					{
						error_message("Non numeric data entered for option -e which requires numeric data.\n");
						exit(1);
					}
					flag->eflg=atoi(optarg);
					if(flag->eflg < 0 || flag->eflg > 1024)
					{
						error_message("The '-e' option setting the type of elements moved must be an integer between 1 and 127\n");
						exit(1);
					}
					if(flag->eflg==0)
						printf("Thats odd, you dont want to move any elements (-e0 option)\n");
			      break;
			 case 'h':
			      flag->hflg=1;
					break;
			 case 'v':
					printf("version = %f\n", version());
					break;
			 case 'd':
					flag->dflg=1;
					break;
			case 'g':
				flag->gflg=atoi(optarg);
				if(flag->gflg<1 || flag->gflg>64)
				{
					fprintf(stderr,"-gx, where x=1 to 64\n");
					exit(1);
				}
				break;
			case 'p':
				flag->pflg=1;
				if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -p which requires numeric data.\n");
						exit(1);
					}
				popsize=atoi(optarg);
				if(popsize < 2 || popsize > 10000000)
				{
						error_message("The '-p' option setting the population size of the genetric algorithm, mush be between 2 amd 1000000\n");
						exit(1);
				}
				break;
			case 't':
				flag->tflg=1;
				if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -t which requires numeric data.\n");
						exit(1);
					}
				length_sd=atof(optarg); /* SD on lengths, in mm */
				if(length_sd < 0 )
				{
						error_message("The '-t' option setting the standard deviation of the element lengths must be > 0\n");
						exit(1);
				}
				break;
			case 'T':
				flag->Tflg=1;
				if(!isdigit( (int) *optarg) && *optarg !='.')
					{
						error_message("Non numeric data entered for option -T which requires numeric data.\n");
						exit(1);
					}
				boom_sd=atof(optarg); /* SD on lengths, in mm */
				if(boom_sd < 0 )
				{
						error_message("The '-T' option setting the standard deviation of the boom postions must be > 0\n");
						exit(1);
				}
				break;
			 case '?':
			   flag->errflg++;
				break;
	 }
	 if(flag->Oflg)
	 {
		flag->oflg-=32768;
	 }
}
