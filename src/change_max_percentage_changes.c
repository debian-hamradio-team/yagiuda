#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <math.h>
#include <stdio.h>
#include "yagi.h"

double change_max_percentage_changes(int i, int iterations, double original_percent)
{
	double pcent=0.0;
	if(original_percent > 0.0)
		{
			if(i<iterations/4)
				pcent=original_percent;
			else if(i>=iterations/4 && i < iterations/2)
				pcent=original_percent/10.0;
			else if (i >=iterations/2 && i < 3*iterations/4)
				pcent=original_percent/100.0;
			else if(i >=3*iterations/4)
				pcent=original_percent/1000.0;
		}
		else if (original_percent < 0.0  )
			pcent =-original_percent;
		return(pcent);
}
