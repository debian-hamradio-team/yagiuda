#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <math.h>
#include <stdio.h>
#include "yagi.h"
/* This function is supposed to allow the user to change the weights etc
while the program is running. Until I can find a way of doing it under
unix, it will stay commented out. */

void dynamic_changing_of_weights(int i, int divisor, struct performance_data *weight)
{
	char c, *str;
	if(i%divisor==0)
	{
		str=string(0L,100L);
		if((fopen("change","rt"))!=NULL)
		{
			system("rm change");
			printf("Enter a letter - G, F, P, R, S or X\n");
			c=getc(stdin);
			switch (c)
			{
				case 'G':
				printf("Enter new weight for gain -current weight=%f\n",weight->gain);
				scanf("%s",str);
				weight->gain=atof(str);
				fflush(stdin);
				break;

				case 'F':
				printf("Enter new weight for FB -current weight=%f\n",weight->fb);
				gets(str);
				weight->fb=atof(str);
				break;

				case 'P':
				printf("Enter new weight for sidelobe -current weight=%f\n",weight->sidelobe);
				gets(str);
				weight->sidelobe=atof(str);
				break;
			} /* end of switch statement */
		} /* end of if file exits/user hit keyboard */
	free_string(str,0L,100L);
	} /* end of if divisor%10==0 */
} /* function  */
