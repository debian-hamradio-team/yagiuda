#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include "yagi.h"

extern int errno;

/* The maximum gain of a Yagi is given in the ARRL handbook, 1991, pp33-17
as 12dB at one wavelength boom length, rising at 2.6 dB 
each time boom lengtrh is doubled */

double determine_maximum_gain(double f, double boom_length_in_m)
{
	double lambda, wavelengths, max_gain;

	lambda=3e8/f;

	wavelengths=boom_length_in_m/lambda; /* boom length in wavelengths */
	if(wavelengths < 1)
		max_gain=8.0;
	else
		max_gain = 12.0 + 2.6*log2(wavelengths);
#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in determine_maximum_gain() of max_gain.c\n", errno);
		exit(1);
	}
#endif
	return(max_gain);
	
}
/* This is another algorithm. Its not as accurate as above, but it gives the
same result form given number of elements. This allows more accurate comparison
of fitnesss'es for the GA and -W algorithm, which would otherwise not be possible. */
double determine_maximum_gain2(int elements)
{
	double wavelengths, max_gain;


	wavelengths=(elements-1)*0.25; /* boom length in wavelengths */
	max_gain = 12.0 + 2.6*log2(wavelengths);
#ifdef DEBUG
	if(errno)
	{
		fprintf(stderr,"Errno =%d in determine_maximum_gain2() of max_gain.c\n", errno);
		exit(1);
	}
#endif
	return(max_gain);
	
}
double log2(double x)
{
	return(log(x)/0.6931471);
}
