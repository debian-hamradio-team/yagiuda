#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include "yagi.h"

extern double xrange_min;
extern double xrange_max;
extern double yrange_min;
extern double yrange_max;
extern double rrange_min;
extern double rrange_max;

void usage_output(char *exefile)
{
		double Zo=Z0;        
		double H_max=H_MAX;
		double E_max=E_MAX;

		fprintf(stderr,"\nYagi-Uda antenna analysi program output, version %.2f\n", version());
		fprintf(stderr,"Written by Dr. David Kirkby Ph.D. G8WRB (email:david.kirkby@onetel.net)\n");
		fprintf(stderr, "\nUSAGE: %s [-cehps] [-E E_max -H H_max -r min -R max -Z Zo] filename \n\n", exefile);
		fprintf(stderr, "Where options are:\n");
		fprintf(stderr, "\n -c  Calculate sidelobe levels (slows program considerably).\n");
		fprintf(stderr, " -e  Suppress calculation of 3dB E-plane BW.\n");
		fprintf(stderr, " -h  Suppress calculation of 3dB H-plane BW.\n");
		fprintf(stderr, " -p  Put data into filename.freq, filename.glog and filename.glin for gnuplot\n");
		fprintf(stderr, " -s  Suppress diagnostic output.\n");
		fprintf(stderr, " -E  Max angle to find the 3dB point. Min=90, max=180  (default = %.0f degrees)\n", E_max);
		fprintf(stderr, " -H  Max angle to find the 3dB point. Min=0, max=90    (default = %.0f degrees)\n", H_max); 
		fprintf(stderr, " -r  Set minimum range on the radial gnuplot log graph (default = %.0f dB)\n", rrange_min);
		fprintf(stderr, " -R  Set maximum range on the radial gnuplot lin graph (default = %.0f dB)\n", rrange_max);
		fprintf(stderr, " -Z  Set characteristic impedance                      (default = %.0f Ohms)\n\n", Zo);
}
