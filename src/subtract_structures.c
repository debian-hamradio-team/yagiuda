#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>
#include "yagi.h"

struct performance_data subtract_structures(struct performance_data a, struct performance_data b)
{
	struct performance_data ans;

	ans.gain=a.gain-b.gain;
	ans.fb=a.fb-b.fb;
	ans.swr=a.swr-b.swr;
	ans.r=a.r-b.r;
	ans.x=a.x-b.x;
	ans.sidelobe=a.sidelobe-b.sidelobe;
	return(ans);
}

