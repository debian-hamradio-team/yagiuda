#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include "yagi.h"

extern int errno;

void test_for_stop_file(void)
{
	if (   fopen("stop","r") !=NULL        )
	{
		fprintf(stderr,"Remove the file \"stop\" first\n");
		exit(1);
	}
	else
		errno=0;
}
