#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif
#include <stdio.h>
#include "yagi.h"

void print_relavent_performance_data(FILE *fp,char *s, int i, struct flags flag , struct performance_data data, double fitness, int Z_Q, int fitness_Q)
{
	if(i!=0)
		fprintf(fp,"%5d ",i);
	else
	{}
	fprintf(fp,"%sG=%5.2fdBi,FB=%6.2fdB,",s,data.gain,data.fb);
	fprintf(fp,"SL=%5.2fdB,",data.sidelobe);		
	fprintf(fp,"SWR=%5.2f",data.swr);
	if(Z_Q==TRUE)
	{
		fprintf(fp,",Z=%6.2f",data.r);
		if(data.x<0.0)
			fprintf(fp,"-j%6.2f",-data.x);
		else
			fprintf(fp,"+j%6.2f",data.x);
	}
	if(fitness_Q==TRUE)
		fprintf(fp,",%.3f",fitness);
	fprintf(fp,"\n");
}
